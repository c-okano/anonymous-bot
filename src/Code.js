function doPost(e) {
  const verificationToken = e.parameter.token;
  if (verificationToken != "★SlackAppのVerificationTokenに書き換えてください★") {
    throw new Error("Invalid token.");
  }

  const slackUrl = "★Incoming WebHooksで発行されたURLに書き換えてください★";

  const payload = {
    "blocks": [
      {
        "type": "divider"
      },
      {
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": "*匿名*\n" + e.parameter.text
        }
      },
      {
        "type": "divider"
      }
    ],
  };

  const params = {
    "method": "POST",
    "headers": { "Content-type": "application/json" },
    "payload": JSON.stringify(payload)
  };

  UrlFetchApp.fetch(slackUrl, params);

  return ContentService.createTextOutput("匿名のメッセージが <★匿名メッセージを投稿する「チャンネルのURL」に書き換えてください★|★匿名メッセージを投稿する「チャンネル名」に書き換えてください。★> に投稿されました");
}
