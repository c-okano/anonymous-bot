# これは、Slackの匿名チャンネルに匿名のメッセージを投稿するやつです

Slash CommandとGASを使って、
Slackの匿名チャンネルに匿名のメッセージを投稿します

## Slackでの使い方

- Slackで `/自分で設定したコマンド 匿名で投稿したいメッセージ` って感じで投稿できます
- どのチャンネルで↑を打っても、必ず匿名チャンネルに投稿されます
- （コマンド間違えた／コマンドつけ忘れたを防ぐためにも自分のDMのチャンネルで使うことをおすすめします）

## こいつを導入する手順

### GASの準備

- Googleドライブを開く
- 左上の新規 → その他 → アプリを追加 → Google Apps Script をインストール
- もう一度、左上の新規 → その他 → Google Apps Script を選択
- Google Apps Script の画面に切り替わる
- 左上の「無題のプロジェクト」を、自分オリジナルのかっこい〜名前に変更する
- コード.gsの中身を全部消す
- src/Code.jsのコードをそのまままるごとコピペ
- メニューの「公開」を選択 → 「ウェブアプリケーションとして導入」を選択
- ３つ目の「Who has access to the app（アプリケーションにアクセスできるユーザー）」を **「Anyone, even anonymous（全員（匿名ユーザーを含む））」** に変更
- 「Deploy（導入）」を選択
- （Authorization required が表示されたら、「許可を確認」を選択）
- Deploy as web app が表示されたら、「Current web app URL」を控えておく（後で使います）
- GASの準備は一旦ここまで

### Slack Appをつくる

- https://api.slack.com/apps にアクセス
- （こいつを導入したいSlackのワークスペースにログインしていなければログインする）
- 「Create New App」ボタンを選択
- 「App Name」にかっこいい名前を入力する（これがSlackに表示されるApp名になります）
- 「Development Slack Workspace」でこいつを導入したいワークスペースを選択する
- 「Create App」ボタンを選択する
- Basic Information の画面に切り替わる
- 「Slash Commands」を選択
- 「Create New Command」を選択
- 「Command」に、匿名で投稿するときに使うコマンドを入力（例：/anonymous）
- 「Request URL」は、GASの準備で控えておいた「Current web app URL」を入力
- 「Short Description」に適当に説明を入力
- 「Usage Hint」も適当に入力
- 「Save」ボタンを選択 → success!
- 左のメニューから「Basic Information」を選択
- 「Incoming Webhooks」を選択
- 「Activate Incoming Webhooks」を On にする
- 「Add New Webhook to Workspace」ボタンを選択
- 匿名でメッセージを投稿するチャンネルを選択する
- 「許可する」ボタンを選択 → success!
- 「Webhook URL」を控えておく（後で使います）
- 左のメニューから「Basic Information」を選択
- 「Add features and functionality」と「Install your app to your workspace」にチェックマークがついている（はず）
- 「Vertification Token」を控えておく（後で使います）
- 「Install your app to your workspace」の「Reinstall App」を選択
- 匿名でメッセージを投稿するチャンネルを選択する
- 「許可する」ボタンを選択 → success!
- Slackの操作は一旦ここまで

### GASのコードを完成させる
- GASに戻る
- 「★SlackAppのVerificationTokenに書き換えてください★」の部分を、控えておいた「Vertification Token」に書き換える
- 「★Incoming WebHooksで発行されたURLに書き換えてください★」の部分を、控えておいた「Webhook URL」に書き換える
- 「★匿名メッセージを投稿する「チャンネルのURL」に書き換えてください★」の部分を、Slackの匿名で投稿する「チャンネルのURL」に書き換える（チャンネル名を右クリック → リンクをコピー）
- 「★匿名メッセージを投稿する「チャンネル名」に書き換えてください。★」の部分を、Slackの匿名で投稿する「チャンネル名」に書き換える
- メニューのファイル → 保存
- メニューの公開 → ウェブアプリケーションとして導入
- **「Project version」を「New」にする**
- 「更新」ボタンを選択
- 操作完了です
